use std::{fmt::Display, fs::File, io::BufReader, path::Path};

#[derive(Debug, PartialEq)]
enum ReaderResponse<'a> {
    Content(&'a str),
    EOF,
}

impl<'a> Display for ReaderResponse<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ReaderResponse::Content(c) => {
                write!(f, "{c}")
            }
            ReaderResponse::EOF => write!(f, "EOF"),
        }
    }
}

trait Reader {
    fn peek(&self, k: usize) -> ReaderResponse;
    fn consume(&mut self, k: usize) -> ReaderResponse;
}

struct InMemory {
    input: String,
    cursor: usize,
}

impl InMemory {
    fn new(path: &str) -> Self {
        let path = Path::new(path);
        let input = std::fs::read_to_string(&path).expect("Could not read source file");
        Self { input, cursor: 0 }
    }
}

impl Reader for InMemory {
    fn peek(&self, k: usize) -> ReaderResponse {
        if self.cursor == self.input.len() {
            return ReaderResponse::EOF;
        }
        let read_to = if self.cursor + k < self.input.len() {
            self.cursor + k
        } else {
            self.input.len()
        };
        ReaderResponse::Content(&self.input[self.cursor..read_to])
    }

    fn consume(&mut self, k: usize) -> ReaderResponse {
        if self.cursor == self.input.len() {
            return ReaderResponse::EOF;
        }
        let read_to = if self.cursor + k < self.input.len() {
            self.cursor + k
        } else {
            self.input.len()
        };
        let response = ReaderResponse::Content(&self.input[self.cursor..read_to]);
        self.cursor = read_to;
        response
    }
}

struct StreamReader {
    buffer: [u8; 4092],
    cursor: usize,
    start: usize,
    end: usize,
    size: usize,
    reader: BufReader<File>,
}

impl StreamReader {
    fn new(path: &str) -> Self {
        let file = File::open(path).expect("Unable to read source file");
        let reader = BufReader::new(file);
        Self {
            buffer: [0; 4092],
            cursor: 0,
            start: 0,
            end: 0,
            size: 0,
            reader,
        }
    }
}

impl Reader for StreamReader {
    fn peek(&self, k: usize) -> ReaderResponse {
        todo!()
    }

    fn consume(&mut self, k: usize) -> ReaderResponse {
        todo!()
    }
}

fn main() {
    let mut reader = InMemory::new("src/main.rs");
    loop {
        let consumed = reader.consume(50);
        println!("Consuming 50 bytes:\n{consumed}");
        if consumed == ReaderResponse::EOF {
            break;
        }
    }
    println!("Consumed the whole thing! Bb now")
}
